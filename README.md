# C++ example with tests

## Build and run
Either open `main.cpp` in juCi++ and choose Compile and Run in the Project menu,
or in a terminal from repository root folder:
```sh
mkdir build
cd build && cmake .. && make && ./example
```

## Build and run tests
Either open `tests/even_test.cpp` in juCi++ and choose Compile and Run in the
Project menu, or in a terminal from repository root folder:
```sh
cd build && make && make test
```
