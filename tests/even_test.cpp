#include "even.hpp"
#include <cassert>

int main() {
  assert(is_even(2));
  assert(is_even(200));

  assert(!is_even(1));
  assert(!is_even(101));
}
