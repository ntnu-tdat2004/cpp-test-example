#include "even.hpp"
#include <iostream>

using namespace std;

int main() {
  cout << "Enter number: " << flush;

  int number;
  cin >> number;

  cout << number << " is ";
  if(is_even(number))
    cout << "even";
  else
    cout << "odd";
  cout << endl;
}
